-- Load LFS image and then cleanup
if file.exists("lfs.lua") then
   local lfsimg = dofile "lfs.lua"
   if file.exists("lfs_lock") then
      file.remove(lfsimg)
      file.remove("lfs_lock")
      file.remove("lfs.lua")
   else
      local f = file.open("lfs_lock", "w")
      f:flush()
      f:close()
      node.flashreload(lfsimg)
   end
end

-- Init LFS
pcall(node.flashindex("_init"))

-- Set up NodeMCU's WiFi
dofile "httpserver-wifi.lua"

-- Start nodemcu-httpsertver
dofile "httpserver-init.lua"

print("Free heap after server init:", node.heap())

-- Set up SDM
dofile "sdm_init.lua"

print("Free heap after SDM init:", node.heap())
