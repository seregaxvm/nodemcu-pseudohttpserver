return function (connection, req, args)
   dofile("httpserver-header.lua")(connection, 200, args.ext, args.isGzipped)

   local arguments = {0} -- function placeholder
   if args.args.call == "public" then
      arguments[#arguments + 1] = sdm.driver_attached(args.dev)
   else
      arguments[#arguments + 1] = args.dev
   end
   local i = 0
   while true do
      local t = args.args["type" .. tostring(i)]
      local d = args.args["data" .. tostring(i)]
      if t == nil or d == nil then break end
      if t == "boolean" then
         arguments[#arguments + 1] = d == "true"
      elseif t == "number" then
         arguments[#arguments + 1] = tonumber(d)
      elseif t == "bytearray" then
         local ba = {}
         for v in d:gmatch("[%d.]+") do
            ba[#ba + 1] = d
         end
         arguments[#arguments + 1] = ba
      else
         arguments[#arguments + 1] = d
      end
      i = i + 1
   end

   if args.args.call == "method" then
      arguments[1] = sdm.method_func(sdm.method_dev_handle(args.dev, args.args.name))
   elseif args.args.call == "local" then
      if args.args.func == "get" then
         arguments[1] = sdm.attr_func(sdm.local_attr_handle(args.dev, args.args.name))
      else
         _, arguments[1] = sdm.attr_func(sdm.local_attr_handle(args.dev, args.args.name))
      end
   elseif args.args.call == "private" then
      if args.args.func == "get" then
         arguments[1] = sdm.attr_func(sdm.attr_dev_handle(args.dev, args.args.name))
      else
         _, arguments[1] = sdm.attr_func(sdm.attr_dev_handle(args.dev, args.args.name))
      end
   elseif args.args.call == "public" then
      if args.args.func == "get" then
         arguments[1] = sdm.attr_func(sdm.attr_drv_handle(sdm.driver_attached(args.dev), args.args.name))
      else
         _, arguments[1] = sdm.attr_func(sdm.attr_drv_handle(sdm.driver_attached(args.dev), args.args.name))
      end
   end

   local rv = ""
   local ok = true
   ok, rv = pcall(unpack(arguments))
   if not ok then
      print("sdm call error: " .. rv)
      rv = "Call error"
   end
   local json = sjson.encode({data=rv})
   connection:send(json)
   arguments = nil
   json = nil
   rv = nil
   ok = nil
   collectgarbage()
end
